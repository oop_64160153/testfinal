/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ubolthip.finaltest;

import javax.swing.ImageIcon;

/**
 *
 * @author OS
 */
public class IceCream {

    private String flavor;
    private String sauce;
    private String topping;
    private String spoon;
    private String lid;
    private String pay;
    private ImageIcon icon;
    private int price;

    public IceCream() {

    }

    public IceCream(String flavor, String sauce, String topping, String spoon, String lid, String pay, ImageIcon icon, int price) {
        this.flavor = flavor;
        this.sauce = sauce;
        this.topping = topping;
        this.spoon = spoon;
        this.lid = lid;
        this.pay = pay;
        this.price = price;
        this.icon = icon;
    }

    public String getFlavor() {
        return flavor;
    }

    public void setFlavor(String flavor) {
        this.flavor = flavor;
    }

    public String getSauce() {
        return sauce;
    }

    public void setSauce(String sauce) {
        this.sauce = sauce;
    }

    public String getTopping() {
        return topping;
    }

    public void setTopping(String topping) {
        this.topping = topping;
    }

    public String getSpoon() {
        return spoon;
    }

    public void setSpoon(String spoon) {
        this.spoon = spoon;
    }

    public String getLid() {
        return lid;
    }

    public void setLid(String lid) {
        this.lid = lid;
    }

    public String getPay() {
        return pay;
    }

    public void setPay(String pay) {
        this.pay = pay;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public ImageIcon getIcon() {
        return icon;
    }

    public void setIcon(ImageIcon icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "IceCream{" + "flavor=" + flavor + ", sauce=" + sauce + ", topping=" + topping + ", spoon=" + spoon + ", lid=" + lid + ", pay=" + pay + ", icon=" + icon + ", price=" + price + '}';
    }

    public String order() {
        String sauceOrder = "-";
        if (!sauce.equals("")) {
            sauceOrder = sauce;
        }
        String toppingOrder = "-";
        if (!topping.equals("")) {
            toppingOrder = topping;
        }        
        return "Flavor: " + flavor + "\nSauce: " + sauceOrder+"\nTopping: "+toppingOrder+"\nPrice: "+price;
    }
}
